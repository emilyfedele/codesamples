#Initial packages
import numpy as np
import pandas as pd
import statsmodels.api as sm #statmodels only has a linear mixed effects model.
import statsmodels.formula.api as smf
import rpy2
import rpy2.robjects #using r interface with python.
r = rpy2.robjects.r
import pandas.rpy.common as com

#Python interface to R
from rpy2.robjects.packages import importr
from rpy2.robjects import Formula
from rpy2.robjects.environments import Environment

#send numpy arrays to R
import rpy2.robjects.numpy2ri
rpy2.robjects.numpy2ri.activate()
from rpy2.robjects import pandas2ri
pandas2ri.activate()

#Other packages for R
utils = importr('utils')
lme4 = importr('lme4')
langR = importr('languageR')

#Get data in right format in pandas//for exposition, collapsing time factor (i.e., ignoring bin, stopping analysis at 800ms after 0).

eye = pd.read_table('engcoh_data_pro_binned.txt',
                    usecols = ['RECORDING_SESSION_LABEL', 'DATA_FILE', 'RIGHT_IN_BLINK', 'RIGHT_IN_SACCADE', 'item_id', 'condition', 'verb_type', '_ID_VALUE', '_ALIGNMENT', 'SUB', 'OBJ', 'LOOKAWAY', 'small_bin', 'big_bin', 'sub_adv'])
df = pd.DataFrame(eye)

df_filtered = df[(df['RIGHT_IN_SACCADE']==0) & (df['RIGHT_IN_BLINK'] == 0) & ((df['condition'] == 'bc-cons') | (df['condition'] == 'fs-cons')) & ((df['small_bin'] == 5) | (df['small_bin'] == 6) | (df['small_bin'] == 7) | (df['small_bin'] == 8))]
right_unique = df_filtered['RIGHT_IN_SACCADE'].unique()
right_blink = df_filtered['RIGHT_IN_BLINK'].unique()
condition_unique = df_filtered['condition'].unique()
bin_unique =  df_filtered['small_bin'].unique()

#pandas DF to r

df_r = pandas2ri.py2ri(df_filtered)

print type(df_r)

#r Enviornment
env = Environment()
for varname in r.colnames(df_r):
    #print varname
    env[varname] = df_r.rx2(varname)

#GLMER
formula = Formula('SUB ~ condition + (1| item_id) + (1|RECORDING_SESSION_LABEL)', environment=env)

model = lme4.glmer(formula,family = 'binomial', verbose=True)
print r.summary(model)













