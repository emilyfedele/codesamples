import pandas as pd
from scipy.stats import kurtosis, kurtosistest
from scipy.stats import skew, skewtest
import matplotlib.pyplot as plt

#1. READ CSV FILE

bot = pd.read_csv('tq-data.csv')
    #print bot['ptid'][:5] #check right format


#2. CREATE DATAFRAME

df = pd.DataFrame(bot)

print df.shape

unique = df.ptid.unique()
print len(unique)

counts = df.groupby(df['ptid']).size()
print counts

    #print df[0:5] #check right format

#3. UNIVARIATE EXPLORE FILE

print df.describe() #prints overall descriptive stats

#3.1 Explore mean/median

print df.mean(numeric_only=True) #mean

print df.median(numeric_only=True) #median, central position of data // Mean values and median values are quite different here

#3.2 Explore std/variance

df_std = df.std() #std

print df_std**2 #variance (std^2)

#3.2.1 compare variance and mean

mean = df.mean(numeric_only=True)
variance = df_std**2

print mean, variance

#3.3 Range

range = df.max(numeric_only=True)-df.min(numeric_only=True)
print range

#3.3.1 Compare Mean, std, range

print mean, df_std, range #std or range too high wrt mean may point to extreme values

#3.4 Measure of normality: skewness/kurtosis -- use scipy.stats to perform test

#KURTOSIS

#looking at variable ACTIVITY; mean/median were very different

#Kurtosis shows peak/tail of data distribution . if k>0: marked peak; if k<0: distribution too flat

k = kurtosis(df['activity'])
zscore, pvalue = kurtosistest(df['activity'])

print "Kurtosis %0.3f z-score %0.3f p-value %0.3f" % (k, zscore, pvalue) #p-value <.05: reject normality

#SKEWNESS

s = skew(df['activity'])
zscore_skew, pvalue_skew = skewtest(df['activity'])

print 'Skewness %0.3f z-score %0.3f p-value %0.3f' % (s, zscore_skew, pvalue_skew)

#4 BIVARIATE EXPLORE FILE

#4.1 Boxplots

df['latency'].plot.box()
plt.show()
#boxplots = df.boxplot(column = ['activity', 'freq', 'latency'], return_type = 'axes')

















