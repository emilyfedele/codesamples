#prompting user for input to determine if given sentence is ambiguous or unambiguous

#open file and read each line

def open_file_read(file):

    with open(file, 'r') as f:
        #read in file line by line
        lines = f.readlines()

    return lines

def display_lines(file):

    ambiguous = []
    unambiguous = []

    lines = open_file_read(file)

    for line in lines:
        print
        print
        print line

        given_input = raw_input("Enter option: a: ambiguous, u: unambiguous, n: next, q: quit")

        if given_input == 'a':
            ambiguous.append(line)
        elif given_input == 'u':
            unambiguous.append(line)
        elif given_input == 'n':
            continue
        elif given_input == 'q':
            break
        else:
            print "Not an option, skipping..."
            continue

    return ambiguous, unambiguous

def make_file(ambiguous, unambiguous):

    all = []

    for line in ambiguous:
        all.append(line + '|1')

    for line in unambiguous:
        all.append(line + '|0')

    with open("classified_pronouns.txt", 'w') as f:
        for line in all:
            f.write(line + '\n')


ambig, unambig = display_lines('final_sentences.txt')
make_file(ambig, unambig)










