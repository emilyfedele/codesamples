import nltk
from nltk.corpus import brown

sentences = brown.tagged_sents() #list of lists of each sentence. Words are tuple: (word, tag)

pronouns = {'PPO', 'PPS', 'PPS+BEZ', 'PPS+HVD', 'PPS+HVZ', 'PPS+MD', 'PPSS', 'PPSS+BER', 'PPSS+HV',
                'PPSS+HVD', 'PPSS+MD'}

#PPO: pronoun, personal, accusative: them it him me us you 'em her thee we'uns
#PPS: pronoun, personal, nominative, 3rd person singular: it he she thee
#PPS+BEZ: pronoun, personal, nominative, 3rd person singular + verb 'to be', present tense, 3rd person singular: it's he's she's
#PPS+HVD: pronoun, personal, nominative, 3rd person singular + verb 'to have', past tense: she'd he'd it'd
#PPS+HVZ: pronoun, personal, nominative, 3rd person singular + verb 'to have', present tense, 3rd person singular: it's he's she's
#PPS+MD: pronoun, personal, nominative, 3rd person singular + modal auxillary: he'll she'll it'll he'd it'd she'd
#PPSS: pronoun, personal, nominative, not 3rd person singular: they we I you ye thou you'uns
#PPSS+BER: pronoun, personal, nominative, not 3rd person singular + verb 'to be', present tense, 2nd person singular or all persons plural: we're you're they're
#PPSS+HV: pronoun, personal, nominative, not 3rd person singular + verb 'to have', uninflected present tense: I've we've they've you've
#PPSS+HVD: pronoun, personal, nominative, not 3rd person singular + verb 'to have', past tense: I'd you'd we'd they'd
#PPSS+MD: pronoun, personal, nominative, not 3rd person singular + modal auxillary: you'll we'll I'll we'd I'd they'll they'd you'd

#looks at sentence (list) to see if tag is in pronoun-tag set
def contains_pronoun(sentence): #returns true/false

    for tup in sentence:
        if tup[1] in pronouns:
            return True

    return False


def get_pronoun_sentences(sentences): #pass in sentences

    pro_sent = []
    for i in range(len(sentences)): #loop through sentences

        sentence = sentences[i] #store each sentence in the sentence var
        if contains_pronoun(sentence): #if that sentence has a pronoun
            pronoun_dict = {}
            pronoun_dict['target_sentence'] = sentence #store that sentence in a dict

            if i != 0:
                pronoun_dict['previous_sentence'] = sentences[i - 1] #store previous sentence in a dict
            pro_sent.append(pronoun_dict) #put dict or pronoun sentences  in list


    return pro_sent #returns a filtered list with dictionaries, each dictionary with target +previous  (word, tag)

def create_sentence(sentence): #takes

    words = []

    for tup in sentence:
        words.append(tup[0])

    return ' '.join(words)

def dict_to_line(dictionary): #concatenates previous + target sentence from dictionary and returns a line

    target = dictionary["target_sentence"]
    previous = dictionary["previous_sentence"]

    target_string = create_sentence(target)
    previous_string = create_sentence(previous)

    return previous_string + ' ' + target_string

def final_lines(my_list): #take list of dictionaries

    final_sentences = []

    for dictionary in my_list:

        final_sentences.append(dict_to_line(dictionary))

    return final_sentences #returns list of sentences

def create_file(my_list):

    with open("final_sentences.txt", 'w') as f:
        for line in my_list:
            f.write(line + '\n')




pronoun_sentences = get_pronoun_sentences(sentences)
final_sentences = final_lines(pronoun_sentences)
create_file(final_sentences)











