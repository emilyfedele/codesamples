import json
import csv

lines = []

#open file, read lines, create file
with open('tq-data.txt','r') as f:
    for line in f:
        line_dict = json.loads(line)
        metric = {'activity' : line_dict['activityMetric']['int'],
                  'freq' : line_dict['frequencySearchMetric']['int'],
                  'latency' : line_dict['latencySearchMetric']['int'],
                  'ptid' : line_dict['ptid']['string'],
                  'ip' : line_dict['userIp']['string']}
        lines.append(metric)
        
#save file as csv
with open('tq-data.csv', 'w') as csvfile:
    fieldnames = ['ptid', 'ip', 'activity', 'freq', 'latency']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    for line in lines:
        writer.writerow(line)

        # Finished


